<!DOCTYPE html>
<html lang="en">
<head>
  <title>UTS WEB2 : Ajax Insert</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <style>
      html,body{
        height: 100%;
      }
  </style>
</head>
<body>
    <?php 
        $conn = new mysqli("localhost","root","","test");
        $sql  = "SELECT * FROM mahasiswa ORDER BY nama DESC ";
        $result = $conn->query($sql);
    ?>
    <div class="container h-100">
      <div class="row align-items-center h-100">
          <div class="col-6 mx-auto jumbotron">
              <form action="action.php" method="POST">
                <div class="form-group">
                  <label for="">NIRM :</label>
                  <input type="text" class="form-control" name="nirm" required>
                </div>
                <div class="form-group">
                  <label for="">Nama Mahasiswa :</label>
                  <input type="text" class="form-control" name="nama" required>
                </div>               
                <button class="btn btn-info">Submit</button>
              </form>
              <br>
              <table  class="table-bordered table ">
                  <thead>
                      <tr>
                         <th>Nirm</th>
                         <th>Nama Mahasiswa</th>
                      </tr>
                  </thead>
                  <tbody>
                     <?php while ($row = $result->fetch_object()) { ?>
                      <tr>
                         <td><?php echo $row->nirm; ?></td>
                         <td><?php echo $row->nama; ?></td>
                      </tr>
                    <?php } ?>
                  </tbody>
              </table>
          </div>
      </div>
    </div>

    <script>
      $(function(){
        $("form").submit(function(e){
          e.preventDefault();

          $.ajax({
            url : "insert.php",
            method : "POST",
            data : $("form").serializeArray(),
            dataType : "json",
            success:function(data){
                $txt = '<tr><td>'+data.nirm+'</td><td>'+data.nama+'</td></tr>';
                $('tbody').prepend($txt);
            }
          });
        });
      });
    </script>
</body>
</html>/